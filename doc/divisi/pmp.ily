\version "2.21.0"

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Poor-man's Partcombine
%
% Author: Rutger Hofman
% Licence: CC-by-SA 4.0.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

voice-prefix = #(define-music-function (voice-id) (integer?)
"Helper"
    (define (voice-converter i)
        (if (zero? i)
            (context-spec-music (make-voice-props-revert) 'Voice)
            (context-spec-music (make-voice-props-set (- i 1)) 'Voice)
        )
    )
    (let ((voice (voice-converter voice-id))
          (dyn (if (zero? voice-id)
                    #{ #}
                    (if (odd? voice-id)
                        #{
                            \temporary \override DynamicText.direction = #UP
                            \temporary \override DynamicLineSpanner.direction = #UP
                        #}
                        #{
                            \temporary \override DynamicText.direction = #DOWN
                            \temporary \override DynamicLineSpanner.direction = #DOWN
                        #}
                    )
                )
          )
         )
        #{ #voice #dyn #}
    )
)

voice-suffix = #(define-music-function (voice-id) (integer?)
"Helper"
    (if (zero? voice-id)
        #{ #}
        #{
            \oneVoice
            \dynamicNeutral
            \revert TextScript.direction
            \revert TextSpanner.direction
        #}
    )
)

voice-line = #(define-music-function (q duration voice-id remove-quoted?)
            (string? ly:music? integer? boolean?)
"Helper to set one quotation for this duration, parameterized"
    (if (not (string-null? q))
        (let ((prefix (voice-prefix voice-id))
              (suffix (voice-suffix voice-id))
             )
            (if remove-quoted?
                #{ #prefix \removeWithTag KEEPQUOTED \quoteDuring #q { #duration } #suffix #}
                #{ #prefix
                    \temporary \override TextScript.direction = #UP
                    \temporary \override TextSpanner.direction = #UP
                    \quoteDuring #q { #duration } #suffix #}
            )
        )
        #{ #}
    )
)

PMPc = #(define-music-function (quotes duration voice-id first-remove-quoted?)
            (list? ly:music? integer? boolean?)
"Helper to set a list of quotations for this duration, parameterized"
    (define (range from n)
        (if (eq? from n) '() (cons from (range (1+ from) n))))
    (define (voice-converter i)
        (if (zero? i)
            (context-spec-music (make-voice-props-revert) 'Voice)
            (context-spec-music (make-voice-props-set (1- i)) 'Voice)))
    (define (create-voice qv)
        (let ((q (car qv))
              (count (car (cdr qv)))
             )
            (voice-line q duration voice-id (or (> count 0) first-remove-quoted?))
        )
    )
    (make-simultaneous-music
        (map create-voice (zip quotes (range 0 (length quotes)))))
)


PMPChords = #(define-music-function (quotes duration) (list? ly:music?)
"Set the quotations in chord mode for this duration"
    (PMPc quotes duration 0 #f)
)


PMPApart = #(define-music-function (quotes duration) (list? ly:music?)
"Set the quotations in multiple-voice mode for this duration. Each quote can
be a list, which is then typeset as chords."
    (define (range from n) (if (eq? from n) '() (cons from (range (1+ from) n))))
    (define (to-list q) (if (list? q) q (list q)))
    (define (create-voice qv)
        (let* ((voice-number (1+ (car (cdr qv))))
               (voice (to-list (car qv)))
               (remove-first-quoted (> voice-number 1))
              )
            (if (= voice-number 1)
                #{ { #(PMPc voice duration voice-number remove-first-quoted) } #}
                #{ \new Voice { #(PMPc voice duration voice-number remove-first-quoted) } #}
                )
            )
        )
    (make-simultaneous-music
        (map create-voice (zip quotes (range 0 (length quotes)))))
)


% Hack to stop a multi-note tremolo at the end of a quotation.
%
stopTremolo = #(define-music-function () ()
    (make-music 'TremoloSpanEvent
        'span-direction STOP))

%{
% Usage example:

music = \relative c'' {
    \repeat tremolo 16 { c32 d | } |
    c4
}

\addQuote music         { \music }

\score {
    \new Staff {
        \quoteDuring music { s1 \stopTremolo | }
    }
}
%}

\score {
