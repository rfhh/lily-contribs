\version "2.21.0"

\addQuote hrnI    { \hrnI }
\addQuote hrnII   { \removeWithTag KEEPQUOTED \hrnII }
\addQuote hrnIII  { \removeWithTag KEEPQUOTED \hrnIII }
\addQuote hrnIV   { \removeWithTag KEEPQUOTED \hrnIV }


hrnAll = <<
\timeline
{
  \transposition f,
  <<
    { \shiftOff \voiceOne \quoteDuring hrnI { s2.*4 } \oneVoice }
    \new Voice { \shiftOff \voiceTwo \quoteDuring hrnII { s2.*4 } }
    \new Voice { \shiftOff \voiceThree \quoteDuring hrnIII { s2.*4 } }
    \new Voice { \shiftOff \voiceThree \quoteDuring hrnIV { s2.*4 } }
  >>
  \barNumberCheck # 5
  % <>^\markup\halign #0.8 { 1.3. } _\markup\halign #1.8 { 2.4. }
  <<
    <<
      { \shiftOff \voiceOne \dynamicUp \quoteDuring hrnI { s2.*4 | } \oneVoice \dynamicNeutral }
      { \shiftOff \quoteDuring hrnIII { s2.*4 | } }
    >>
    \new Voice <<
      \voiceTwo
      { \shiftOff \quoteDuring hrnII { s2.*4 | } }
      { \shiftOff \quoteDuring hrnIV { s2.*4 | } }
    >>
  >>
  \barNumberCheck # 9
  <>_\markup\halign #2.3 \override #'(baseline-skip . 2.0) \center-column { "1.3." "2.4." }
  <<
    { \shiftOff \quoteDuring hrnI { s2. | } }
    { \shiftOff \quoteDuring hrnII { s2. | } }
    % III is unisono with I
    % IV is unisono with II
  >>
  \barNumberCheck # 10
  <<
    { \shiftOff \quoteDuring hrnI { s2. | s4 } }
    { \shiftOff \quoteDuring hrnII { s2. | s4 } }
    { \shiftOff \quoteDuring hrnIII { s2. | s4 } }
    { \shiftOff \quoteDuring hrnIV { s2. | s4 } }
  >>
  % Two pairs of quotations: chorded hrnI and hrnIII in the \voiceOne
  % voice, a new Voice has chorded hrnII and hrnIV in the \voiceTwo voice.
  \barNumberCheck # 11
  <<
    <<
      { \shiftOff \voiceOne \dynamicUp \quoteDuring hrnI { s2 | } \oneVoice \dynamicNeutral }
      { \shiftOff \quoteDuring hrnIII { s2 | } }
    >>
    \new Voice <<
      \voiceTwo
      { \shiftOff \quoteDuring hrnII { s2 <>\! | } }
      { \shiftOff \quoteDuring hrnIV { s2 <>\! | } }
    >>
  >>
  \barNumberCheck # 12
  <<
    { \shiftOff \quoteDuring hrnI { s2. | } }
    { \shiftOff \quoteDuring hrnII { s2. | } }
    { \shiftOff \quoteDuring hrnIII { s2. | } }
    { \shiftOff \quoteDuring hrnIV { s2. | } }
  >>
  \barNumberCheck # 13
}
>>
