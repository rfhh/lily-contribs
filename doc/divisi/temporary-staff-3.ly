\version "2.21.0"

% shortcuts for temporary staff visibility

tempStaffOff = {
  \stopStaff
  \omit Staff.KeySignature \omit Staff.TimeSignature \omit Staff.Clef
}

tempStaffOn = {
  \startStaff
  \undo \omit Staff.KeySignature \undo \omit Staff.TimeSignature \undo \omit Staff.Clef
}

tutti = \relative c'' {
  \repeat unfold 20 { \repeat unfold 4 a4 } |
}

solo = \relative c'' {
  \override VoiceFollower.bound-details.right.arrow = ##t
  \tempStaffOff
  s1*8 |
  s2. \change Staff = tutti
  % draw arrow from tutti to solo
    \once \hideNotes d4 |
    \once \showStaffSwitch \change Staff = solo
  \tempStaffOn
  \clef treble
  <>^"1.Solo" \repeat unfold 8 c4
  <>^"Con altri"
  \tempStaffOff
  % draw arrow from solo to tutti
  \once \showStaffSwitch \change Staff = tutti
    \once \hideNotes d4 s2. |
  s1*8 |
}

\addQuote tutti { \tutti }

\score {
  <<
    \new Staff = solo \with {
      \RemoveAllEmptyStaves
      instrumentName = "1.Solo"
    } \solo
    \new Staff = tutti \with {
      \RemoveAllEmptyStaves
      shortInstrumentName = "Altr."
    } \tutti
  >>
}
