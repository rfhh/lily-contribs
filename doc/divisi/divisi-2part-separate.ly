\version "2.21.0"

\include "divisi-2part-notes.ily"


\score {
  \new GrandStaff <<
    \new Staff = fluteI \with {
    } <<
      \fluteI
      \exampleBreaks
    >>
    \new Staff = fluteII \with {
    } {
      \fluteII
    }
  >>

  \header {
    piece = \markup\bold\fontsize #2 { separate staves }
  }
}
