\version "2.21.0"

% Put this into your library
targetstaff = #(define-scheme-function (ctx) (string?)
  #{
    \set Staff.keepAliveInterfaces = #'()
    \context Staff = #ctx { \unset Staff.keepAliveInterfaces }
  #})


\paper {
    indent = 6
    short-indent = 6
}

\layout {
  \context {
    \Score {
      \override Score.BarNumber.break-visibility = ##(#f #t #t)
    }
  }
}


timeline = {
  \time 3/4
  \bar ""
  s2.*4 |
  \barNumberCheck # 5
  \bar "||"
  s2.*4 |
  \barNumberCheck # 9
  \bar "||"
  s2.*4 |
  \barNumberCheck # 13
  \bar "|."
}

hrnIa = \relative c'' {
  \repeat unfold 4 { e,2( b'4) | }
}

hrnIIa = \relative c'' {
  \repeat unfold 4 { c2( f,4) | }
}

hrnIIIa = \relative c'' {
  \repeat unfold 4 { a4( e'2) | }
}

hrnIVa = \relative c'' {
  \repeat unfold 4 { f4( d2) | }
}


hrnIb = \relative c'' {
  <>^"staccato"
  \repeat unfold 4 { e,8 g c e c g | }
}

hrnIIb = \relative c'' {
  \tag KEEPQUOTED <>^"staccato"
  \repeat unfold 4 { a8 c f c a f | }
}

hrnIIIb = \relative c'' {
  \tag KEEPQUOTED <>^"staccato"
  \repeat unfold 4 { c,8 e g c g e | }
}

hrnIVb= \relative c'' {
  \tag KEEPQUOTED <>^"staccato"
  \repeat unfold 4 { f,8 a c a f c | }
}


hrnIc = \relative c'' {
  cis,2. |
  r8 c\p\< d e f g |
  a4\! d8\f\> d4 d8 |
  d2.\! |
}

hrnIIc = \relative c'' {
  bes,2. |
  r8 a\p\< b c d e |
  f4\! f\f\> f |
  f2.\! |
}

hrnIIIc = \relative c'' {
  cis,2. |
  r8 b\p\< c d e f |
  g4\! c8\f\> c4 c8 |
  c2.\! |
}

hrnIVc = \relative c'' {
  bes,2. |
  r8 g\p\< a b c d |
  e4\! e\f\> e |
  e2.\! |
}


hrnI = <<
\timeline
{
  \transposition f,
  \hrnIa \hrnIb \hrnIc
}
>>

hrnII = <<
\timeline
{
  \transposition f,
  \hrnIIa \hrnIIb \hrnIIc
}
>>

hrnIII = <<
\timeline
{
  \transposition f,
  \hrnIIIa \hrnIIIb \hrnIIIc
}
>>

hrnIV = <<
\timeline
{
  \transposition f,
  \hrnIVa \hrnIVb \hrnIVc
}
>>
