\version "2.21.0"

tutti = \relative c'' {
  \repeat unfold 20 { \repeat unfold 4 a4 } |
}

solo = \relative c'' {
  \override VoiceFollower.bound-details.right.arrow = ##t
  s1*8 |
  s2. \change Staff = tutti
  % draw arrow from tutti to solo
    \once \hideNotes d4
    \once \showStaffSwitch \change Staff = solo |
  <>^"1.Solo" \repeat unfold 8 c4
  <>^"Con altri"
  % draw arrow from solo to tutti
  \once \showStaffSwitch \change Staff = tutti
    \once \hideNotes d4 s2. |
  s1*8 |
}

\addQuote tutti { \tutti }

\score {
  <<
    \new Staff = solo \with {
      \RemoveAllEmptyStaves
      instrumentName = "1.Solo"
      shortInstrumentName = "1.Solo"
    } \solo
    \new Staff = tutti \with {
      \RemoveAllEmptyStaves
      shortInstrumentName = "Altr."
    } \tutti
  >>
}
