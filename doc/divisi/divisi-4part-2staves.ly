\version "2.21.0"

\include "divisi-4part-notes.ily"


\score {
  \keepWithTag #'( SCORE KEEPQUOTED ) <<
    \new StaffGroup <<
      \new Staff = hrnIeIII \with {
        instrumentName = "1.3."
        shortInstrumentName = "1.3."
      } <<
        \partCombine
          \hrnI
          \removeWithTag KEEPQUOTED \hrnIII
      >>
      \new Staff = hrnIIeIV \with {
        instrumentName = "2.4."
        shortInstrumentName = "2.4."
      } <<
        \partCombine
          \hrnII
          \removeWithTag KEEPQUOTED \hrnIV
      >>
    >>
  >>

  \header {
    piece = "On two staves"
  }
}
