\version "2.21.0"

\include "divisi-4part-notes.ily"
\include "divisi-4part-quotes.ily"

\score {
  \new Staff = hrnAll \with {
    instrumentName = \markup\column{ "1.3." "2.4." }
    shortInstrumentName = \markup\column{ "1.3." "2.4." }
  } <<
    \hrnAll
  >>

  \header {
    piece = "On one staff"
  }
}
