\version "2.21.0"

\include "divisi-4part-notes.ily"
\include "divisi-4part-quotes.ily"

breaks = <<
\timeline
{
  s2.*5 |
  \barNumberCheck # 6
  \break
  s2.*4 |
  \barNumberCheck # 10
  \break
  s2.*3 |
  \barNumberCheck # 13
}
>>

hrnDivisi = <<
\timeline
{
  % select collection of 4 staves
  \targetstaff hrnI
  s2.*4 |
  % select collection of 2 staves
  \targetstaff hrnIeIII
  s2.*4 |
  % select collection of 1 staff
  \targetstaff hrnAll
  s2.*4 |
  \barNumberCheck # 13
}
>>


\score {
  \keepWithTag #'( SCORE KEEPQUOTED ) <<
    \new Dynamics \breaks

    \new StaffGroup \with {
      \consists "Keep_alive_together_engraver"
    } <<
      % collection of 1 staff
      \new Staff = hrnAll \with {
        instrumentName = \markup\column{ "1.3." "2.4." }
        shortInstrumentName = \markup\column{ "1.3." "2.4." }
        \RemoveAllEmptyStaves
        \override VerticalAxisGroup.remove-layer = #80
      } <<
        \hrnAll
        \hrnDivisi
      >>

      % collection of 2 staves
      \new Staff = hrnIeIII \with {
        instrumentName = "1.3."
        shortInstrumentName = "1.3."
        \RemoveAllEmptyStaves
        \override VerticalAxisGroup.remove-layer = #40
      } <<
        \partCombine
          \hrnI
          \removeWithTag KEEPQUOTED \hrnIII
        \hrnDivisi
      >>
      \new Staff = hrnIIeIV \with {
        instrumentName = "2.4."
        shortInstrumentName = "2.4."
        \RemoveAllEmptyStaves
        \override VerticalAxisGroup.remove-layer = #40
      } <<
        \partCombine
          \hrnII
          \removeWithTag KEEPQUOTED \hrnIV
        \hrnDivisi
      >>

      % collection of 4 staves
      \new Staff = hrnI \with {
        instrumentName = "1."
        shortInstrumentName = "1."
        \RemoveAllEmptyStaves
        \override VerticalAxisGroup.remove-layer = #20
      } <<
        \hrnI
        \hrnDivisi
      >>
      \new Staff = hrnII \with {
        instrumentName = "2."
        shortInstrumentName = "2."
        \RemoveAllEmptyStaves
        \override VerticalAxisGroup.remove-layer = #20
      } <<
        \hrnII
        \hrnDivisi
      >>
      \new Staff = hrnIII \with {
        instrumentName = "3."
        shortInstrumentName = "3."
        \RemoveAllEmptyStaves
        \override VerticalAxisGroup.remove-layer = #20
      } <<
        \hrnIII
        \hrnDivisi
      >>
      \new Staff = hrnIV \with {
        instrumentName = "4."
        shortInstrumentName = "4."
        \RemoveAllEmptyStaves
        \override VerticalAxisGroup.remove-layer = #20
      } <<
        \hrnIV
        \hrnDivisi
      >>
    >>
  >>

  \header {
    piece = "Flexible divisi"
  }
}
