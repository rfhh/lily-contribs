\version "2.21.0"

\include "divisi-2part-notes.ily"

\score {
  \new Staff \with {
  } <<
    \exampleBreaks
    \partCombine \fluteI \fluteII
  >>

  \header {
    piece = \markup\bold\fontsize #2 { always combined }
  }
}
