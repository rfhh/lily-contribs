\version "2.21.0"

% Put this into your library
targetstaff = #(define-scheme-function (ctx) (string?)
  #{
    \set Staff.keepAliveInterfaces = #'()
    \context Staff = #ctx { \unset Staff.keepAliveInterfaces }
  #})


\include "divisi-2part-notes.ily"

fluteDivisi = {
  \targetstaff fluteIeII
  s1*7 |
  \targetstaff fluteI
  s1*2 |
  \targetstaff fluteIeII
  s1*5 |
}


\score {
  \new GrandStaff \with {
    \consists "Keep_alive_together_engraver"
  } <<
    % Collection that has the flutes on 1 staff
    \new Staff = fluteIeII \with {
      \RemoveAllEmptyStaves
      \override VerticalAxisGroup.remove-layer = 40
    } <<
      \partCombine \fluteI \fluteII
      \fluteDivisi
      \exampleBreaks
    >>

    % Collection that has the flutes on 2 staves
    \new Staff = fluteI \with {
      \RemoveAllEmptyStaves
      \override VerticalAxisGroup.remove-layer = 20
    } <<
      \fluteI
      \fluteDivisi
    >>
    \new Staff = fluteII \with {
      \RemoveAllEmptyStaves
      \override VerticalAxisGroup.remove-layer = 20
    } <<
      \fluteII
      \fluteDivisi
    >>
  >>

  \header {
    piece = \markup\bold\fontsize #2 { flexible divisi }
  }
}


