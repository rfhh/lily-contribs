\version "2.21.0"

% Put this into your library
targetstaff = #(define-scheme-function (ctx) (string?)
  #{
    \set Staff.keepAliveInterfaces = #'()
    \context Staff = #ctx { \unset Staff.keepAliveInterfaces }
  #})


% notes

vI = \relative c'' {
  \repeat unfold 7 { \repeat unfold 4 c4 | }
  <>^"div.a 5"
  \repeat unfold 2 { \repeat unfold 4 e4 | }
  <>^"unis."
  \repeat unfold 7 { \repeat unfold 4 c4 | }
}

vII = \relative c'' {
  \quoteDuring vI { s1*7 | }
  \repeat unfold 2 { c4 d c b | }
  \quoteDuring vI { s1*7 | }
}

vIII = \relative c'' {
  \quoteDuring vI { s1*7 | }
  \repeat unfold 2 { b4 a g a | }
  \quoteDuring vI { s1*7 | }
}

vIV = \relative c'' {
  \quoteDuring vI { s1*7 | }
  \repeat unfold 2 { g4 f g f | }
  \quoteDuring vI { s1*7 | }
}

vV = \relative c'' {
  \quoteDuring vI { s1*7 | }
  \repeat unfold 2 { f,4 e d f | }
  \quoteDuring vI { s1*7 | }
}

exampleBreaks = {
  s1*6 |
  \break
  s1*5 |
  \break
}


\addQuote vI    { \vI }
\addQuote vII   { \vII }
\addQuote vIII  { \vIII }
\addQuote vIV   { \vIV }
\addQuote vV    { \vV }


vAll = {
  <<
    { \shiftOff \oneVoice \quoteDuring vI { s1*16 | } }
    { \shiftOff \oneVoice \quoteDuring vII { s1*16 | } }
    { \shiftOff \oneVoice \quoteDuring vIII { s1*16 | } }
    { \shiftOff \oneVoice \quoteDuring vIV { s1*16 | } }
    { \shiftOff \oneVoice \quoteDuring vV { s1*16 | } }
  >>
}


\score {
  \new Staff <<
    \vAll
    \exampleBreaks
  >>

  \header {
    piece = \markup\bold\fontsize #2 { Combined onto one staff }
  }
}


vDivisi = {
  % select the 1-staff collection
  \targetstaff vAll
  s1*7 |
  % select one of the staves in the 5-staff collection
  \targetstaff vI
  s1*2 |
  % select the 1-staff collection
  \targetstaff vAll
  s1*7 |
}

\score {
  \new GrandStaff \with {
    \consists "Keep_alive_together_engraver"
  } <<
    % collection with one staff
    \new Staff = vAll \with {
      \RemoveAllEmptyStaves
      \override VerticalAxisGroup.remove-layer = #40
    } <<
      \vAll
      \exampleBreaks
    >>

    % collection with five staves
    \new Staff = vI \with {
      \RemoveAllEmptyStaves
      \override VerticalAxisGroup.remove-layer = #20
    } <<
      \vI
      \vDivisi
    >>
    \new Staff = vII \with {
      \RemoveAllEmptyStaves
      \override VerticalAxisGroup.remove-layer = #20
    } <<
      \vII
      \vDivisi
    >>
    \new Staff = vIII \with {
      \RemoveAllEmptyStaves
      \override VerticalAxisGroup.remove-layer = #20
    } <<
      \vIII
      \vDivisi
    >>
    \new Staff = vIV \with {
      \RemoveAllEmptyStaves
      \override VerticalAxisGroup.remove-layer = #20
    } <<
      \vIV
      \vDivisi
    >>
    \new Staff = vV \with {
      \RemoveAllEmptyStaves
      \override VerticalAxisGroup.remove-layer = #20
    } <<
      \vV
      \vDivisi
    >>
  >>

  \header {
    piece = \markup\bold\fontsize #2 { Divisi onto one or five staves }
  }
}
