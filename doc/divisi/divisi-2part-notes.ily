\version "2.21.0"

fluteI = \relative c'' {
  \repeat unfold 14 \repeat unfold 4 b4 |
}

fluteII = \relative c'' {
  \repeat unfold 7 { \repeat unfold 4 g4 | }
  \repeat unfold 2 { e'4 e, e' e, | }
  \repeat unfold 5 { \repeat unfold 4 g4 | }
}

exampleBreaks = {
  s1*6 |
  \break
}
