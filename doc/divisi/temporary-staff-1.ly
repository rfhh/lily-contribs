\version "2.21.0"

% Put these into your library
simple = \set Staff.keepAliveInterfaces = #'()
tricky = \unset Staff.keepAliveInterfaces

tutti = \relative c'' {
  \transposition c
  \repeat unfold 20 { \repeat unfold 4 a4 } |
}

solo = \relative c'' {
  \transposition c
  \simple
  \quoteDuring tutti {
    s1*9 |
  }
  \tricky
  <>^"1.Solo"
  \repeat unfold 8 c4
  \simple
  <>^"Con altri"
  \quoteDuring tutti {
    s1*9 |
  }
}

\addQuote tutti { \tutti }

\score {
  <<
    \new Staff = solo \with {
      \RemoveAllEmptyStaves
      instrumentName = "1.Solo"
      shortInstrumentName = "1.Solo"
    } \solo
    \new Staff = tutti \with {
      \RemoveAllEmptyStaves
      shortInstrumentName = "Altr."
    } \tutti
  >>
}
