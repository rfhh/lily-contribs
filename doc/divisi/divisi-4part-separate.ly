\version "2.21.0"

\include "divisi-4part-notes.ily"

\score {
  \new StaffGroup <<
    \new Staff = hrnI \with {
      instrumentName = "1."
      shortInstrumentName = "1."
    } {
      \hrnI
    }
    \new Staff = hrnII \with {
      instrumentName = "2."
      shortInstrumentName = "2."
    } {
      \hrnII
    }
    \new Staff = hrnIII \with {
      instrumentName = "3."
      shortInstrumentName = "3."
    } {
      \hrnIII
    }
    \new Staff = hrnIV \with {
      instrumentName = "4."
      shortInstrumentName = "4."
    } {
      \hrnIV
    }
  >>

  \header {
    piece = "On four staves"
  }
}
